package com.example.a3buttons;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class button3 extends AppCompatActivity {
    private Button buttonclose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button3);
        buttonclose = (Button)findViewById(R.id.close2);
        buttonclose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)             {

                Intent intent = new Intent(button3.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}