package com.example.a3buttons;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class button2 extends AppCompatActivity {
    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button2);
       button = (Button)findViewById(R.id.exit);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)             {

                Intent intent = new Intent(button2.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }
}